import React, {useState, useEffect} from "react";
import {store} from './firebase'

function App() {
  const [modoEdicion, setModoEdicion] = useState(null);
  const [idUsuario, setIdUsuario] = useState('');
  const[nombre,setNombre] = useState('');
  const[phone, setPhone] = useState('');
  const [error, setError] = useState('')
  const[usuario, setUsuario] = useState([]);

  useEffect(()=>{
    const getUsuarios = async()=>{
      const { docs } = await store.collection('agenda').get();
      const nuevoArray = docs.map( item => ({id:item.id, ...item.data()}) )
      setUsuario(nuevoArray);
    }
    getUsuarios();
  },[])

  const setUsuarios = async (e) => {
    e.preventDefault();
    if(!nombre.trim()){
      setError('El campo nombre está vacio')
    }
    if(!phone.trim()){
      setError('El campo teléfono está vacio')
    }
    const usuario = {
      nombre:nombre,
      telefono:phone
    }
    try{
      const data = await store.collection('agenda').add(usuario);
      const { docs } = await store.collection('agenda').get();
      const nuevoArray = docs.map( item => ({id:item.id, ...item.data()}) )
      setUsuario(nuevoArray);
      alert('Usuario añadido');
    }catch(e){
      console.log(e)
    }
    setNombre('');
    setPhone('');

  }

  const borrarUsuario = async (id) => {
    try{
      await store.collection('agenda').doc(id).delete()
      const { docs } = await store.collection('agenda').get();
      const nuevoArray = docs.map( item => ({id:item.id, ...item.data()}) )
      setUsuario(nuevoArray);
    }catch(e){
      console.log(e);
    }
  }

  const pulsarActualizar = async (id)=>{
    try {
      const data = await store.collection('agenda').doc(id).get();
      const { nombre, telefono} = data.data();
      setNombre(nombre);
      setPhone(telefono);
      setIdUsuario(id);
      setModoEdicion(true);
    }catch(e){
      console.log(e);
    }
  }

  const setUpdate = async (e) => {
    e.preventDefault();
    if(!nombre.trim()){
      setError('El campo nombre está vacio')
    }
    if(!phone.trim()){
      setError('El campo teléfono está vacio')
    }
    const usuario = {
      nombre:nombre,
      telefono:phone
    }
    const userUpdate = {
      nombre:nombre,
      telefono:phone
    }
    try{
      await store.collection('agenda').doc(idUsuario).set(userUpdate);
      const { docs } = await store.collection('agenda').get();
      const nuevoArray = docs.map( item => ({id:item.id, ...item.data()}) )
      setUsuario(nuevoArray);
    }catch(e){
      console.log(e);
    }
    setNombre('');
    setPhone('');
    setIdUsuario('');
    setModoEdicion('');


  }
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <h2>Formulario de Usuarios</h2>
          <form onSubmit={modoEdicion ? setUpdate : setUsuarios} className="form-group">
            <input
            value = {nombre}
              onChange={(e)=>{setNombre(e.target.value)}}
              className="form-control"
              placeholder="Introduce el nombre"
              type="text"
            />
            <input
            value = {phone}
              onChange={(e)=>{setPhone(e.target.value)}}
              className="form-control mt-3"
              placeholder="Introduce el numero"
              type="text"
            />
              {
              modoEdicion ?
              (
                <input
                type="submit"
                value="Editar"
                className="btn btn-dark btn-block mt-3"
              />
              )
              :
              (
                <input
                type="submit"
                value="Registrar"
                className="btn btn-dark btn-block mt-3"
              />
              )
              }

          </form>
          {
            error ?
            (
              <div>
                <p>{error}</p>
              </div>
            )
            :
            (
              <span></span>
            )
          }
        </div>
        <div className="col">
          <h2>Lista de tu agenda</h2>
          <ul className="list-group">
          {
            usuario.length !== 0 ?
            (
              usuario.map(item =>(
                <li className="list-group-item" key={item.id}>{item.nombre} -- {item.telefono} 
                  <button onClick={(id)=> {borrarUsuario(item.id)}} className="btn btn-danger float-right">ELIMINAR</button>
                  <button onClick={(id)=> {pulsarActualizar(item.id)}} className="btn btn-info mr-3 float-right">ACTUALIZAR</button>
                </li>
              ))
            )
            :
            (
              <span>
                No hay usuarios  
              </span>
            )
          }
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;
