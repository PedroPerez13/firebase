import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyBHqyZXWWQqaWpRVafm4Plv5yuenSUApZs",
    authDomain: "fir-autenticacion-a74cc.firebaseapp.com",
    projectId: "fir-autenticacion-a74cc",
    storageBucket: "fir-autenticacion-a74cc.appspot.com",
    messagingSenderId: "960767829420",
    appId: "1:960767829420:web:e9b155204e53fb8d9b0609"
  };
  // Initialize Firebase
  const fireb = firebase.initializeApp(firebaseConfig);
  const store = fireb.firestore();

  export {store};